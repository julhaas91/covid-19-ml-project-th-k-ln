import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np


def prepare_dataframe(df, name='', save=False):
    """Prepares a DataFrame to our specific requirements"""
    df.drop(columns='Lat', inplace=True)
    df.drop(columns='Long', inplace=True)
    df.drop(columns='Province/State', inplace=True)
    df = df.groupby('Country/Region').sum().reset_index()
    df = df.transpose()
    df = df.rename(columns=df.iloc[0])
    df = df.drop(df.index[0])
    df.index = pd.to_datetime(df.index)
    if save:
        df.to_csv(name)
    return df


def sel_highest_n(df, n):
    """Selects the n DataFrame entries with highest amounts of Corona Cases, to avoid cluttering the graphs"""
    df.sort_values(by="2020-05-03", axis=1, ascending=False, inplace=True)
    df = df.iloc[:, :n]
    return df


def show_graph(path, top=0):
    """Shows the specified graph (optional: select only the n top countries to be graphed"""
    df = prepare_dataframe(pd.read_csv(path))
    plt.style.use('fivethirtyeight')
    if top != 0:
        ax1 = sel_highest_n(df, top).plot()
    else:
        ax1 = df.plot()
    plt.ticklabel_format(style='plain', axis='y')
    ax1.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    plt.show()


def show_country(country="Germany", plot=True):
    """Graphs confirmed, recovered and deaths for a country."""
    con = prepare_dataframe(pd.read_csv('time_series_confirmed.csv'))
    dea = prepare_dataframe(pd.read_csv('time_series_deaths.csv'))
    rec = prepare_dataframe(pd.read_csv('time_series_recovered.csv'))
    country_data = [con[country], dea[country], rec[country]]
    df = pd.concat(country_data, axis=1, keys=["confirmed cases", "deaths", "recovered"])
    print(df.head())
    if plot:
        ax1 = df.plot(color=['b','r','g'])
        #plt.style.use('fivethirtyeight')
        plt.title("Covid-19 Data from " + country)
        plt.ticklabel_format(style='plain', axis='y')
        ax1.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
        plt.show()
    return df

def plot_HDI_confirmed():
    """Plots the HDI correlated with the Confirmed cases"""
    con = prepare_dataframe(pd.read_csv('time_series_confirmed.csv'))
    hdi = pd.read_csv('HDI.csv')
    hdi.columns = ['Country', 'HDI', 'Population2020']
    hdi = hdi.set_index('Country')

    con = con.max()
    df = pd.concat([hdi, con], axis=1)
    df.columns = ['HDI', 'Population2020', 'confirmed']
    df.dropna(axis=0, how="any", inplace=True)

    plt.subplot(2,1,1)
    plt.scatter(df.HDI, df.confirmed)
    plt.title("HDI vs. current maximum confirmed cases for a country (04.05.2020")
    plt.xlabel("HDI")
    plt.ylabel("Confirmed cases")
    plt.subplot(2,1,2)
    plt.scatter(df.Population2020, df.confirmed)
    plt.title("Population vs. current maximum confirmed cases for a country (04.05.2020")
    plt.xlabel("Population (billion)")
    plt.ylabel("Confirmed cases")
    plt.show()

def standartize_IQAIR():
    datelist = pd.date_range(start='2020-02-15', end='2020-05-02').tolist()
    IQAIR = pd.read_csv('IQAir Dataset.csv', delimiter=';')

    repeat_datelist = []
    repeat_value = []
    repeat_country = []
    for country in range(IQAIR.shape[0]):
        for date in datelist:
            repeat_country.append(IQAIR.iloc[country, 0])
            repeat_datelist.append(date)
            repeat_value.append(IQAIR.iloc[country, 1])
    df = pd.DataFrame(np.column_stack([repeat_country, repeat_datelist, repeat_value]),columns=['country', 'date', 'pm2.5 value'])
    print(df.head())
    df = df.set_index(['country', 'date'])
    df.to_csv('IQAIR_standartized.csv')

def standartize_healthspendings():
    spendings = pd.read_csv('health_spendings.CSV', index_col='location_name')
    spendings = spendings[spendings['year']==2017]
    spendings = spendings.loc[:,['the_total_mean']]
    spendings = spendings.loc['Afghanistan':, :]
    spendings = spendings.reset_index()
    print(spendings.head(20))

    datelist = pd.date_range(start='2020-02-15', end='2020-05-02').tolist()
    repeat_datelist = []
    repeat_value = []
    repeat_country = []
    for country in range(spendings.shape[0]):
        for date in datelist:
            repeat_country.append(spendings.iloc[country, 0])
            repeat_datelist.append(date)
            repeat_value.append(spendings.iloc[country, 1])
    df = pd.DataFrame(np.column_stack([repeat_country, repeat_datelist, repeat_value]),columns=['country', 'date', 'mean_health_spendings'])
    print(df.head())
    df = df.set_index(['country', 'date'])
    df.to_csv('spendings_standartized.csv')


#show_graph('time_series_confirmed.csv', top=15)
#show_country(country='Germany', plot=False)
#plot_HDI_confirmed()
#standartize_IQAIR()
standartize_healthspendings()