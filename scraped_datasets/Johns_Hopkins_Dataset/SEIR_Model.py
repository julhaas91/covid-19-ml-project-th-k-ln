from scipy.integrate import odeint
import datetime
import matplotlib.pyplot as plt
import numpy as np
import EDAGeneralCorona as EDA
import pandas as pd


# Recovery rate in days: current estimate of full recovery time is 14 days
def make_SEIRModel_fn(rse, rrs, rei, rir):
    # assert(r_ir+r_rs == 1.)
    def SEIR_node(S, time):
        S[S < 0] = 0.

        xs, xe, xi, xr = S
        rse_xsi = rse * xs * xi
        rei_xe = rei * xe
        rrs_xr = rrs * xr
        rir_xi = rir * xi
        d_xs = - rse_xsi + rrs_xr
        d_xe = + rse_xsi - rei_xe
        d_xi = + rei_xe - rir_xi
        d_xr = rir_xi - rrs_xr

        return [d_xs, d_xe, d_xi, d_xr]

    return SEIR_node


def plot_SEIR_model_for_parameters(r_se, r_rs, r_ei, r_ir, initial_exposed, idf, time):
    """
    r_se: susceptible to exposed probability
    r_rs: recovered to susceptible. This is probably quite small.
    r_ei: exposed to infectious. That we actually more-or-less know: it's the inverse of the incubation period (5 to 14 days)
    r_ir:
    """

    seir = make_SEIRModel_fn(r_se / idf, r_rs, r_ei, r_ir)

    res = odeint(seir, [1 - initial_exposed, initial_exposed, 0.0, 0.0], np.linspace(0, time, 1000))
    total_healthy = res[:, 0] + res[:, -1]
    total_sick = res[:, 1] + res[:, 2]

    plt.plot(np.linspace(0, time, 1000), res)
    plt.title(
        "SEIR Parameters: \n" + r"$ r_{S \rightarrow E} $= %2.3f ,   $r_{R \rightarrow S}$: %2.3f , $r_{R \rightarrow S}$: %2.3f, $r_{R \rightarrow S}$: %2.3f" % (
        r_se, r_rs, r_ei, r_ir) + "\n" + r"Spread prevention: %2.4f pct, peak $x_I$: %2.4f pct" % (
        (idf - 1) * 100, np.max(res[:, 3]) * 100), fontsize=20)
    plt.legend([r"$x_%s$ %s" % (m[1], m) for m in ["(susceptible)", "(exposed)", "(infectious)", "(recovered)"]],
                 fontsize=20)
    plt.grid()
    plt.ylim([0, 1])


    return res, total_healthy, total_sick

def plot_curr_SEIR(r_se=0.24, r_rs=1/3.5, r_ei=1 / 5, r_ir=0.11, initial_exposed=0.0025, idf=2):
    """Compares SEIR model with real data TODO: ONLY FOR GERMANY"""
    df = EDA.show_country(country='Germany', plot=False)
    df.drop(columns='deaths', inplace=True)
    real = df[df["confirmed cases"] >= 0.0001*83000000]
    real_scaled = real / 83000000
    seir = make_SEIRModel_fn(r_se / idf, r_rs, r_ei, r_ir)

    res = odeint(seir, [1 - initial_exposed, initial_exposed, 0.0, 0.0], np.linspace(0, real.shape[0], real.shape[0]))
    resdf = pd.DataFrame(res, columns=["susceptible", "exposed", "infectious", "recovered"],index=real.index)
    resdf.drop(columns='susceptible', inplace=True)
    resdf.drop(columns='exposed', inplace=True)
    print(resdf.head())

    plt.plot(resdf)
    plt.plot(real_scaled)


    plt.show()



#plt.figure(figsize=(20, 10))
#plot_SEIR_model_for_parameters(r_se=0.2, r_rs=0.05, r_ei=1/14, r_ir=0.11, initial_exposed=0.05, idf=4, time=63)
#EDA.show_country()
#plt.show()
plot_curr_SEIR()