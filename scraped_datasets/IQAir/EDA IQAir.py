import pandas as pd
import matplotlib.pyplot as plt

IQAIR = pd.read_csv('IQAir Dataset.csv', delimiter=';', index_col='Country', )
colors = []

for i in range(IQAIR.shape[0]):
    if IQAIR["Pm2.5 value (2019)"].iloc[i]<= 10:
        colors.append('b')
    elif IQAIR["Pm2.5 value (2019)"].iloc[i]>= 38:
        colors.append('r')
    else:
        colors.append('g')

y_pos = [i*3 for i in range(IQAIR.shape[0])]
plt.barh(y_pos, list(IQAIR["Pm2.5 value (2019)"]), color=colors, height=2)
plt.yticks(y_pos, list(IQAIR.index), fontsize=5)
plt.axvline(10)

plt.title('Air pollution based on pm2.5 value. Vertical line is the WHO goal')
plt.show()