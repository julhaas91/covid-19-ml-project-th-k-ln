import pandas as pd

def standartize_tested():
    tested = pd.read_csv('amounts_tested.csv')
    tested = tested[['Entity','Date', 'Cumulative total','Daily change in cumulative total', 'Cumulative total per thousand']]
    tested['Entity'] = tested['Entity'].str.partition(' - ')[0]
    tested.columns=['country','Date', 'Cumulative total','Daily change in cumulative total', 'Cumulative total per thousand']
    duplicate_indices = [list(range(1507, 1531)), list(range(1763, 1789)), list(range(1870, 1947)), list(range(3650, 3656)), list(range(4209, 4215)),list(range(4509, 4548)), list(range(4616, 4618))]

    for indices in duplicate_indices:
        tested.drop(indices, inplace=True)

    #tested.to_csv('standartized_tested.csv')

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
standartize_tested()